<img src="https://atomjump.com/images/logo80.png">

# doctor-patient
Patient discussion on shared messaging forum

This is the front-end HTML/Javascript software used on ajmp.co, to create a unique forum identifier that can be shared between a doctor and patient for a chat window using the AtomJump Messaging server.

